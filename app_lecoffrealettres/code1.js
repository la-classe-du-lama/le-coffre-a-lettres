gdjs.menuCode = {};
gdjs.menuCode.localVariables = [];
gdjs.menuCode.GDtitreObjects1= [];
gdjs.menuCode.GDtitreObjects2= [];
gdjs.menuCode.GDtitreObjects3= [];
gdjs.menuCode.GDniveau_9595facileObjects1= [];
gdjs.menuCode.GDniveau_9595facileObjects2= [];
gdjs.menuCode.GDniveau_9595facileObjects3= [];
gdjs.menuCode.GDniveau_9595moyenObjects1= [];
gdjs.menuCode.GDniveau_9595moyenObjects2= [];
gdjs.menuCode.GDniveau_9595moyenObjects3= [];
gdjs.menuCode.GDniveau_9595difficileObjects1= [];
gdjs.menuCode.GDniveau_9595difficileObjects2= [];
gdjs.menuCode.GDniveau_9595difficileObjects3= [];
gdjs.menuCode.GDsp_9595cadre1Objects1= [];
gdjs.menuCode.GDsp_9595cadre1Objects2= [];
gdjs.menuCode.GDsp_9595cadre1Objects3= [];
gdjs.menuCode.GDsp_9595cadre2Objects1= [];
gdjs.menuCode.GDsp_9595cadre2Objects2= [];
gdjs.menuCode.GDsp_9595cadre2Objects3= [];
gdjs.menuCode.GDsp_9595cadre3Objects1= [];
gdjs.menuCode.GDsp_9595cadre3Objects2= [];
gdjs.menuCode.GDsp_9595cadre3Objects3= [];
gdjs.menuCode.GDsergeObjects1= [];
gdjs.menuCode.GDsergeObjects2= [];
gdjs.menuCode.GDsergeObjects3= [];
gdjs.menuCode.GDsp_9595titreObjects1= [];
gdjs.menuCode.GDsp_9595titreObjects2= [];
gdjs.menuCode.GDsp_9595titreObjects3= [];
gdjs.menuCode.GDsp_9595bouton_9595configObjects1= [];
gdjs.menuCode.GDsp_9595bouton_9595configObjects2= [];
gdjs.menuCode.GDsp_9595bouton_9595configObjects3= [];
gdjs.menuCode.GDsp_9595bouton_9595infoObjects1= [];
gdjs.menuCode.GDsp_9595bouton_9595infoObjects2= [];
gdjs.menuCode.GDsp_9595bouton_9595infoObjects3= [];
gdjs.menuCode.GDversionObjects1= [];
gdjs.menuCode.GDversionObjects2= [];
gdjs.menuCode.GDversionObjects3= [];
gdjs.menuCode.GDserge_9595lamaObjects1= [];
gdjs.menuCode.GDserge_9595lamaObjects2= [];
gdjs.menuCode.GDserge_9595lamaObjects3= [];
gdjs.menuCode.GDauteurObjects1= [];
gdjs.menuCode.GDauteurObjects2= [];
gdjs.menuCode.GDauteurObjects3= [];
gdjs.menuCode.GDlicenceObjects1= [];
gdjs.menuCode.GDlicenceObjects2= [];
gdjs.menuCode.GDlicenceObjects3= [];
gdjs.menuCode.GDinfosObjects1= [];
gdjs.menuCode.GDinfosObjects2= [];
gdjs.menuCode.GDinfosObjects3= [];
gdjs.menuCode.GDpolicesObjects1= [];
gdjs.menuCode.GDpolicesObjects2= [];
gdjs.menuCode.GDpolicesObjects3= [];
gdjs.menuCode.GDbouton_9595retourObjects1= [];
gdjs.menuCode.GDbouton_9595retourObjects2= [];
gdjs.menuCode.GDbouton_9595retourObjects3= [];
gdjs.menuCode.GDcoffreObjects1= [];
gdjs.menuCode.GDcoffreObjects2= [];
gdjs.menuCode.GDcoffreObjects3= [];
gdjs.menuCode.GDsp_9595diamantObjects1= [];
gdjs.menuCode.GDsp_9595diamantObjects2= [];
gdjs.menuCode.GDsp_9595diamantObjects3= [];
gdjs.menuCode.GDGreenDotBarObjects1= [];
gdjs.menuCode.GDGreenDotBarObjects2= [];
gdjs.menuCode.GDGreenDotBarObjects3= [];
gdjs.menuCode.GDsp_9595cadre0Objects1= [];
gdjs.menuCode.GDsp_9595cadre0Objects2= [];
gdjs.menuCode.GDsp_9595cadre0Objects3= [];
gdjs.menuCode.GDpapierObjects1= [];
gdjs.menuCode.GDpapierObjects2= [];
gdjs.menuCode.GDpapierObjects3= [];
gdjs.menuCode.GDtext_9595modeleObjects1= [];
gdjs.menuCode.GDtext_9595modeleObjects2= [];
gdjs.menuCode.GDtext_9595modeleObjects3= [];
gdjs.menuCode.GDclavierObjects1= [];
gdjs.menuCode.GDclavierObjects2= [];
gdjs.menuCode.GDclavierObjects3= [];


gdjs.menuCode.eventsList0 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.storage.elementExistsInJSONFile("sauvegarde_lecoffrealettres", "reglages");
if (isConditionTrue_0) {
{gdjs.evtTools.storage.readStringFromJSONFile("sauvegarde_lecoffrealettres", "reglages", runtimeScene, runtimeScene.getScene().getVariables().get("sauvegarde"));
}{gdjs.evtTools.network.jsonToVariableStructure(gdjs.evtTools.variable.getVariableString(runtimeScene.getScene().getVariables().get("sauvegarde")), runtimeScene.getGame().getVariables().getFromIndex(1));
}}

}


};gdjs.menuCode.eventsList1 = function(runtimeScene) {

{


gdjs.menuCode.eventsList0(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}}

}


};gdjs.menuCode.eventsList2 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("GreenDotBar"), gdjs.menuCode.GDGreenDotBarObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.menuCode.GDGreenDotBarObjects2.length;i<l;++i) {
    if ( gdjs.menuCode.GDGreenDotBarObjects2[i].getVariableNumber(gdjs.menuCode.GDGreenDotBarObjects2[i].getVariables().getFromIndex(0)) == 1 ) {
        isConditionTrue_0 = true;
        gdjs.menuCode.GDGreenDotBarObjects2[k] = gdjs.menuCode.GDGreenDotBarObjects2[i];
        ++k;
    }
}
gdjs.menuCode.GDGreenDotBarObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.menuCode.GDGreenDotBarObjects2 */
{for(var i = 0, len = gdjs.menuCode.GDGreenDotBarObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDGreenDotBarObjects2[i].SetValue(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("score").getChild("niv1").getAsNumber(), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("GreenDotBar"), gdjs.menuCode.GDGreenDotBarObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.menuCode.GDGreenDotBarObjects2.length;i<l;++i) {
    if ( gdjs.menuCode.GDGreenDotBarObjects2[i].getVariableNumber(gdjs.menuCode.GDGreenDotBarObjects2[i].getVariables().getFromIndex(0)) == 2 ) {
        isConditionTrue_0 = true;
        gdjs.menuCode.GDGreenDotBarObjects2[k] = gdjs.menuCode.GDGreenDotBarObjects2[i];
        ++k;
    }
}
gdjs.menuCode.GDGreenDotBarObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.menuCode.GDGreenDotBarObjects2 */
{for(var i = 0, len = gdjs.menuCode.GDGreenDotBarObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDGreenDotBarObjects2[i].SetValue(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("score").getChild("niv2").getAsNumber(), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("GreenDotBar"), gdjs.menuCode.GDGreenDotBarObjects2);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.menuCode.GDGreenDotBarObjects2.length;i<l;++i) {
    if ( gdjs.menuCode.GDGreenDotBarObjects2[i].getVariableNumber(gdjs.menuCode.GDGreenDotBarObjects2[i].getVariables().getFromIndex(0)) == 3 ) {
        isConditionTrue_0 = true;
        gdjs.menuCode.GDGreenDotBarObjects2[k] = gdjs.menuCode.GDGreenDotBarObjects2[i];
        ++k;
    }
}
gdjs.menuCode.GDGreenDotBarObjects2.length = k;
if (isConditionTrue_0) {
/* Reuse gdjs.menuCode.GDGreenDotBarObjects2 */
{for(var i = 0, len = gdjs.menuCode.GDGreenDotBarObjects2.length ;i < len;++i) {
    gdjs.menuCode.GDGreenDotBarObjects2[i].SetValue(runtimeScene.getGame().getVariables().getFromIndex(1).getChild("score").getChild("niv3").getAsNumber(), (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{


let isConditionTrue_0 = false;
{
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(0);
}}

}


};gdjs.menuCode.eventsList3 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(0)) == 1;
if (isConditionTrue_0) {

{ //Subevents
gdjs.menuCode.eventsList2(runtimeScene);} //End of subevents
}

}


};gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsp_95959595cadre1Objects1Objects = Hashtable.newFrom({"sp_cadre1": gdjs.menuCode.GDsp_9595cadre1Objects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsp_95959595cadre2Objects1Objects = Hashtable.newFrom({"sp_cadre2": gdjs.menuCode.GDsp_9595cadre2Objects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsp_95959595cadre3Objects1Objects = Hashtable.newFrom({"sp_cadre3": gdjs.menuCode.GDsp_9595cadre3Objects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsp_95959595bouton_95959595configObjects1Objects = Hashtable.newFrom({"sp_bouton_config": gdjs.menuCode.GDsp_9595bouton_9595configObjects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsp_95959595bouton_95959595infoObjects1Objects = Hashtable.newFrom({"sp_bouton_info": gdjs.menuCode.GDsp_9595bouton_9595infoObjects1});
gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDserge_95959595lamaObjects1Objects = Hashtable.newFrom({"serge_lama": gdjs.menuCode.GDserge_9595lamaObjects1});
gdjs.menuCode.eventsList4 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
{
{gdjs.evtTools.storage.clearJSONFile("sauvegarde_lecoffrealettres");
}{gdjs.evtTools.storage.writeStringInJSONFile("sauvegarde_lecoffrealettres", "reglages", gdjs.evtTools.network.variableStructureToJSON(runtimeScene.getGame().getVariables().getFromIndex(1)));
}}

}


};gdjs.menuCode.eventsList5 = function(runtimeScene) {

{


gdjs.menuCode.eventsList4(runtimeScene);
}


{


let isConditionTrue_0 = false;
{
{runtimeScene.getScene().getVariables().getFromIndex(0).setNumber(1);
}}

}


};gdjs.menuCode.eventsList6 = function(runtimeScene) {

{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "chrono");
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 1, false, 100, 1);
}{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/audio_vide.mp3", 2, false, 100, 1);
}
{ //Subevents
gdjs.menuCode.eventsList1(runtimeScene);} //End of subevents
}

}


{


gdjs.menuCode.eventsList3(runtimeScene);
}


{



}


{

gdjs.copyArray(runtimeScene.getObjects("sp_cadre1"), gdjs.menuCode.GDsp_9595cadre1Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsp_95959595cadre1Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_cadre2"), gdjs.menuCode.GDsp_9595cadre2Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsp_95959595cadre2Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(2);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_cadre3"), gdjs.menuCode.GDsp_9595cadre3Objects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsp_95959595cadre3Objects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(3);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "jeu", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_config"), gdjs.menuCode.GDsp_9595bouton_9595configObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsp_95959595bouton_95959595configObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") >= 0.2;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "code", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("sp_bouton_info"), gdjs.menuCode.GDsp_9595bouton_9595infoObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDsp_95959595bouton_95959595infoObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.getTimerElapsedTimeInSecondsOrNaN(runtimeScene, "chrono") >= 0.2;
}
}
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "infos", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("serge_lama"), gdjs.menuCode.GDserge_9595lamaObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.cursorOnObject(gdjs.menuCode.mapOfGDgdjs_9546menuCode_9546GDserge_95959595lamaObjects1Objects, runtimeScene, true, false);
if (isConditionTrue_0) {
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}
if (isConditionTrue_0) {
{gdjs.evtTools.sound.playMusicOnChannel(runtimeScene, "audio/bulle_1.mp3", 1, false, 100, 1);
}{runtimeScene.getScene().getVariables().getFromIndex(1).add(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().getFromIndex(1)) >= 5;
if (isConditionTrue_0) {
{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("score").getChild("niv1").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("score").getChild("niv2").setNumber(0);
}{runtimeScene.getGame().getVariables().getFromIndex(1).getChild("score").getChild("niv3").setNumber(0);
}
{ //Subevents
gdjs.menuCode.eventsList5(runtimeScene);} //End of subevents
}

}


};

gdjs.menuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.menuCode.GDtitreObjects1.length = 0;
gdjs.menuCode.GDtitreObjects2.length = 0;
gdjs.menuCode.GDtitreObjects3.length = 0;
gdjs.menuCode.GDniveau_9595facileObjects1.length = 0;
gdjs.menuCode.GDniveau_9595facileObjects2.length = 0;
gdjs.menuCode.GDniveau_9595facileObjects3.length = 0;
gdjs.menuCode.GDniveau_9595moyenObjects1.length = 0;
gdjs.menuCode.GDniveau_9595moyenObjects2.length = 0;
gdjs.menuCode.GDniveau_9595moyenObjects3.length = 0;
gdjs.menuCode.GDniveau_9595difficileObjects1.length = 0;
gdjs.menuCode.GDniveau_9595difficileObjects2.length = 0;
gdjs.menuCode.GDniveau_9595difficileObjects3.length = 0;
gdjs.menuCode.GDsp_9595cadre1Objects1.length = 0;
gdjs.menuCode.GDsp_9595cadre1Objects2.length = 0;
gdjs.menuCode.GDsp_9595cadre1Objects3.length = 0;
gdjs.menuCode.GDsp_9595cadre2Objects1.length = 0;
gdjs.menuCode.GDsp_9595cadre2Objects2.length = 0;
gdjs.menuCode.GDsp_9595cadre2Objects3.length = 0;
gdjs.menuCode.GDsp_9595cadre3Objects1.length = 0;
gdjs.menuCode.GDsp_9595cadre3Objects2.length = 0;
gdjs.menuCode.GDsp_9595cadre3Objects3.length = 0;
gdjs.menuCode.GDsergeObjects1.length = 0;
gdjs.menuCode.GDsergeObjects2.length = 0;
gdjs.menuCode.GDsergeObjects3.length = 0;
gdjs.menuCode.GDsp_9595titreObjects1.length = 0;
gdjs.menuCode.GDsp_9595titreObjects2.length = 0;
gdjs.menuCode.GDsp_9595titreObjects3.length = 0;
gdjs.menuCode.GDsp_9595bouton_9595configObjects1.length = 0;
gdjs.menuCode.GDsp_9595bouton_9595configObjects2.length = 0;
gdjs.menuCode.GDsp_9595bouton_9595configObjects3.length = 0;
gdjs.menuCode.GDsp_9595bouton_9595infoObjects1.length = 0;
gdjs.menuCode.GDsp_9595bouton_9595infoObjects2.length = 0;
gdjs.menuCode.GDsp_9595bouton_9595infoObjects3.length = 0;
gdjs.menuCode.GDversionObjects1.length = 0;
gdjs.menuCode.GDversionObjects2.length = 0;
gdjs.menuCode.GDversionObjects3.length = 0;
gdjs.menuCode.GDserge_9595lamaObjects1.length = 0;
gdjs.menuCode.GDserge_9595lamaObjects2.length = 0;
gdjs.menuCode.GDserge_9595lamaObjects3.length = 0;
gdjs.menuCode.GDauteurObjects1.length = 0;
gdjs.menuCode.GDauteurObjects2.length = 0;
gdjs.menuCode.GDauteurObjects3.length = 0;
gdjs.menuCode.GDlicenceObjects1.length = 0;
gdjs.menuCode.GDlicenceObjects2.length = 0;
gdjs.menuCode.GDlicenceObjects3.length = 0;
gdjs.menuCode.GDinfosObjects1.length = 0;
gdjs.menuCode.GDinfosObjects2.length = 0;
gdjs.menuCode.GDinfosObjects3.length = 0;
gdjs.menuCode.GDpolicesObjects1.length = 0;
gdjs.menuCode.GDpolicesObjects2.length = 0;
gdjs.menuCode.GDpolicesObjects3.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects1.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects2.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects3.length = 0;
gdjs.menuCode.GDcoffreObjects1.length = 0;
gdjs.menuCode.GDcoffreObjects2.length = 0;
gdjs.menuCode.GDcoffreObjects3.length = 0;
gdjs.menuCode.GDsp_9595diamantObjects1.length = 0;
gdjs.menuCode.GDsp_9595diamantObjects2.length = 0;
gdjs.menuCode.GDsp_9595diamantObjects3.length = 0;
gdjs.menuCode.GDGreenDotBarObjects1.length = 0;
gdjs.menuCode.GDGreenDotBarObjects2.length = 0;
gdjs.menuCode.GDGreenDotBarObjects3.length = 0;
gdjs.menuCode.GDsp_9595cadre0Objects1.length = 0;
gdjs.menuCode.GDsp_9595cadre0Objects2.length = 0;
gdjs.menuCode.GDsp_9595cadre0Objects3.length = 0;
gdjs.menuCode.GDpapierObjects1.length = 0;
gdjs.menuCode.GDpapierObjects2.length = 0;
gdjs.menuCode.GDpapierObjects3.length = 0;
gdjs.menuCode.GDtext_9595modeleObjects1.length = 0;
gdjs.menuCode.GDtext_9595modeleObjects2.length = 0;
gdjs.menuCode.GDtext_9595modeleObjects3.length = 0;
gdjs.menuCode.GDclavierObjects1.length = 0;
gdjs.menuCode.GDclavierObjects2.length = 0;
gdjs.menuCode.GDclavierObjects3.length = 0;

gdjs.menuCode.eventsList6(runtimeScene);
gdjs.menuCode.GDtitreObjects1.length = 0;
gdjs.menuCode.GDtitreObjects2.length = 0;
gdjs.menuCode.GDtitreObjects3.length = 0;
gdjs.menuCode.GDniveau_9595facileObjects1.length = 0;
gdjs.menuCode.GDniveau_9595facileObjects2.length = 0;
gdjs.menuCode.GDniveau_9595facileObjects3.length = 0;
gdjs.menuCode.GDniveau_9595moyenObjects1.length = 0;
gdjs.menuCode.GDniveau_9595moyenObjects2.length = 0;
gdjs.menuCode.GDniveau_9595moyenObjects3.length = 0;
gdjs.menuCode.GDniveau_9595difficileObjects1.length = 0;
gdjs.menuCode.GDniveau_9595difficileObjects2.length = 0;
gdjs.menuCode.GDniveau_9595difficileObjects3.length = 0;
gdjs.menuCode.GDsp_9595cadre1Objects1.length = 0;
gdjs.menuCode.GDsp_9595cadre1Objects2.length = 0;
gdjs.menuCode.GDsp_9595cadre1Objects3.length = 0;
gdjs.menuCode.GDsp_9595cadre2Objects1.length = 0;
gdjs.menuCode.GDsp_9595cadre2Objects2.length = 0;
gdjs.menuCode.GDsp_9595cadre2Objects3.length = 0;
gdjs.menuCode.GDsp_9595cadre3Objects1.length = 0;
gdjs.menuCode.GDsp_9595cadre3Objects2.length = 0;
gdjs.menuCode.GDsp_9595cadre3Objects3.length = 0;
gdjs.menuCode.GDsergeObjects1.length = 0;
gdjs.menuCode.GDsergeObjects2.length = 0;
gdjs.menuCode.GDsergeObjects3.length = 0;
gdjs.menuCode.GDsp_9595titreObjects1.length = 0;
gdjs.menuCode.GDsp_9595titreObjects2.length = 0;
gdjs.menuCode.GDsp_9595titreObjects3.length = 0;
gdjs.menuCode.GDsp_9595bouton_9595configObjects1.length = 0;
gdjs.menuCode.GDsp_9595bouton_9595configObjects2.length = 0;
gdjs.menuCode.GDsp_9595bouton_9595configObjects3.length = 0;
gdjs.menuCode.GDsp_9595bouton_9595infoObjects1.length = 0;
gdjs.menuCode.GDsp_9595bouton_9595infoObjects2.length = 0;
gdjs.menuCode.GDsp_9595bouton_9595infoObjects3.length = 0;
gdjs.menuCode.GDversionObjects1.length = 0;
gdjs.menuCode.GDversionObjects2.length = 0;
gdjs.menuCode.GDversionObjects3.length = 0;
gdjs.menuCode.GDserge_9595lamaObjects1.length = 0;
gdjs.menuCode.GDserge_9595lamaObjects2.length = 0;
gdjs.menuCode.GDserge_9595lamaObjects3.length = 0;
gdjs.menuCode.GDauteurObjects1.length = 0;
gdjs.menuCode.GDauteurObjects2.length = 0;
gdjs.menuCode.GDauteurObjects3.length = 0;
gdjs.menuCode.GDlicenceObjects1.length = 0;
gdjs.menuCode.GDlicenceObjects2.length = 0;
gdjs.menuCode.GDlicenceObjects3.length = 0;
gdjs.menuCode.GDinfosObjects1.length = 0;
gdjs.menuCode.GDinfosObjects2.length = 0;
gdjs.menuCode.GDinfosObjects3.length = 0;
gdjs.menuCode.GDpolicesObjects1.length = 0;
gdjs.menuCode.GDpolicesObjects2.length = 0;
gdjs.menuCode.GDpolicesObjects3.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects1.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects2.length = 0;
gdjs.menuCode.GDbouton_9595retourObjects3.length = 0;
gdjs.menuCode.GDcoffreObjects1.length = 0;
gdjs.menuCode.GDcoffreObjects2.length = 0;
gdjs.menuCode.GDcoffreObjects3.length = 0;
gdjs.menuCode.GDsp_9595diamantObjects1.length = 0;
gdjs.menuCode.GDsp_9595diamantObjects2.length = 0;
gdjs.menuCode.GDsp_9595diamantObjects3.length = 0;
gdjs.menuCode.GDGreenDotBarObjects1.length = 0;
gdjs.menuCode.GDGreenDotBarObjects2.length = 0;
gdjs.menuCode.GDGreenDotBarObjects3.length = 0;
gdjs.menuCode.GDsp_9595cadre0Objects1.length = 0;
gdjs.menuCode.GDsp_9595cadre0Objects2.length = 0;
gdjs.menuCode.GDsp_9595cadre0Objects3.length = 0;
gdjs.menuCode.GDpapierObjects1.length = 0;
gdjs.menuCode.GDpapierObjects2.length = 0;
gdjs.menuCode.GDpapierObjects3.length = 0;
gdjs.menuCode.GDtext_9595modeleObjects1.length = 0;
gdjs.menuCode.GDtext_9595modeleObjects2.length = 0;
gdjs.menuCode.GDtext_9595modeleObjects3.length = 0;
gdjs.menuCode.GDclavierObjects1.length = 0;
gdjs.menuCode.GDclavierObjects2.length = 0;
gdjs.menuCode.GDclavierObjects3.length = 0;


return;

}

gdjs['menuCode'] = gdjs.menuCode;
